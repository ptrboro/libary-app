## README

# See working app here:
https://morning-wildwood-65709.herokuapp.com/

# Clone app repository: 
```
git clone git@bitbucket.org:ptrboro/libary-app.git
```
# Install gems:
```
bundle install
```
# Migrate database:
```
rails db:migrate
```
# Update secrets.yml 
Fill google_client_id and google_client_secret with your creditials

# Run tests:
```
rspec
```

# Start server:
```
rails s
```

# Production
In production you have to set environment variables for google:
GOOGLE_CLIENT_ID
GOOGLE_CLIENT_SECRET

and for AWS:
S3_ACCESS_KEY
S3_SECRET_KEY
S3_REGION
S3_BUCKET