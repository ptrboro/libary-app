class BooksController < ApplicationController
  before_action :set_book, only: [:show, :edit, :update, :destroy]
  before_action :only_logged_in
  before_action :check_authorization, only: [:edit, :destroy]
  
  # /books
  def index
    @borrowed_books = current_user.borrowed_books
    @available_books = Book.available
    @books = Book.all
  end
  
  # /books/:id
  def show
    @book_events = @book.book_events.order(created_at: :desc).paginate(page: params[:page], per_page: 5)
  end
  
  # /books/new
  def new
    if params[:book_url]
      if @book = Book.get_book_from_lubimy_czytac(params[:book_url])
        flash[:success] = "Getting book details successed"
      else
        flash[:warning] = "Getting book details failed"
        @book = Book.new
      end
    else
      @book = Book.new
    end
  end
  
  # /books/:id/edit
  def edit
  end
  
  # /books
  def create
    @book = current_user.added_books.build(book_params)

    if @book.save
      flash[:success] = 'Book was successfully created.'
      redirect_to @book
    else
      render :new
    end
  end
  
  # /books/:id
  def update
    if params['commit'] == "Borrow"
      if @book.borrow_by(current_user)
        flash[:success] = "You borrowed this book!"
        redirect_back fallback_location: root_path
      end
    elsif params['commit'] == "Return"
      if @book.current_reader == current_user && @book.return_book
        flash[:success] = 'Book was successfully returned'
        redirect_back fallback_location: root_path
      else
        redirect_to root_path
      end
    else
      if @book.update(book_params)
        flash[:success] = 'Book was successfully updated.'
        redirect_to @book
      else
          render :edit
      end
    end
  end
  # /book/:id
  def destroy
    @book.destroy
    flash[:success] = 'Book was successfully destroyed.' 
    redirect_to books_url
  end

  private
    # Make code more DRY
    def set_book
      @book = Book.find(params[:id])
    end
    
    # Strong params
    def book_params
      params.require(:book).permit(:title, :author, :description, :added_by_id, :cover_image_url, :temporary_cover_image_url)
    end
    
    # Only logged in users can access actions, not logged in will be redirected to login path
    def only_logged_in
      redirect_to '/login' if current_user.nil?
    end
    
    # Prevent users from updating/deleting not their books
    def check_authorization
      book = Book.find(params[:id])
      redirect_to root_path unless book.user == current_user
    end
end
