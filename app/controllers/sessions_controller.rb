class SessionsController < ApplicationController
    
    # /auth/:provider
    def create
      begin 
        @user = User.from_omniauth(request.env['omniauth.auth'])
        session[:user_id] = @user.id
        flash[:success] = "Welcome, #{@user.name}!"
      rescue # When athentication fails add warning message
        flash[:warning] = "There was an error while trying to authenticate you..."
      end
      redirect_to root_path
    end
    
    # /logout 
    def destroy
      if current_user
        session.delete(:user_id)
        flash[:success] = 'See you!'
      end
      redirect_to root_path
    end
    
    # used by OmniAuth
    def auth_failure
      redirect_to root_path
    end
end
