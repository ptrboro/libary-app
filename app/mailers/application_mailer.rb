class ApplicationMailer < ActionMailer::Base
  default from: 'noreply@libary.com'
  layout 'mailer'
end
