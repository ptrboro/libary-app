class BookMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.book_mailer.reminder.subject
  #
  def reminder(book)
    @book = book
    mail to: @book.current_reader.email, subject: "Book reminder"
    
  end
end
