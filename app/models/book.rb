class Book < ApplicationRecord
  belongs_to :user, foreign_key: :added_by_id
  has_many :book_events, dependent: :destroy
  after_create :set_cover_image_url
  
  
  validates :title, presence: true
  validates :author, presence: true
  validates :description, presence: true
  validate :upload_cover_image_url
  
  attr_accessor :temporary_cover_image_url
  
  # Add cover image uploader
  mount_uploader :cover_image_url, BookCoverUploader

  
  # Order books by title ascending by default
  default_scope { order(title: :asc) }
  
  def self.get_book_from_lubimy_czytac(url)
    return false unless url.include?('lubimyczytac.pl')
    doc = Nokogiri::HTML(open(url))
    book = self.new
    book.title = doc.css('.grid_5').css('.alpha').css('.omega').css('>h1').try(:text)
    book.author = doc.css('.grid_5').css('.alpha').css('.omega').css('>div>span')[0].try(:text)
    book.description = doc.css('#sBookDescriptionLong')[0].try(:text).try(:squish)
    book.temporary_cover_image_url = doc.css('.nyroModal>img')[0].attributes['src'].value

    book
  end
  
  # returns all unavailable books
  def self.unavailable
    joins(:book_events).where('book_events.return_date' => nil)
  end
  
  # returns array of all available books
  def self.available
    all - unavailable
  end
  
  # returns true if book is available
  def available?
    return true if self.book_events.empty?
    self.book_events.last.returned?
  end
  
  # returns user who has this book
  def current_reader
    return nil if self.available?
    self.book_events.last.user
  end
  
  # Returns book back to libary
  #   Updates last BookEvent return_date with current date
  def return_book
    self.book_events.last.update_attribute(:return_date, Date.today)
  end
  
  # Borrow book by specific user
  # creates new BookEvent
  def borrow_by(user)
    return false if !self.available?
    self.book_events.build(user: user, borrow_date: Date.today).save
  end
  
  # Send reminders in background
  def self.send_reminders
    books = unavailable.where('book_events.borrow_date' => Date.today - 10.days)
    books.each do |book|
      BookMailer.reminder(book).deliver
    end
  end
  
  private 
    def upload_cover_image_url
       errors.add(:base, "Cover image must be added") if self.cover_image_url.file.nil? and self.temporary_cover_image_url.nil?
    end
  
    def set_cover_image_url
      if self.temporary_cover_image_url
        self.update_attribute(:remote_cover_image_url_url, self.temporary_cover_image_url)
        if self.reload.cover_image_url.file.nil?
          self.destroy
        end
      end
    end
end
