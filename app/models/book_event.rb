class BookEvent < ApplicationRecord
  belongs_to :book
  belongs_to :user
  
  validates :borrow_date, presence: true
  
  def returned?
    !self.return_date.nil?
  end
end
