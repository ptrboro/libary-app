class User < ApplicationRecord
    
    validates :name, presence: true
    
    has_many :added_books, class_name: "Book", foreign_key: :added_by_id
    has_many :book_events, -> { where return_date: nil }
    has_many :borrowed_books, through: :book_events, source: :book
    
    # User borrows specific book
    #  (creates new BookEvent with book set to specific book and borrow_date set to current date)
    def borrow_book(book)
        return false if !book.available?
        self.book_events.build(book: book, borrow_date: Date.today).save
    end
    
    
    # Returns user from OmniAuth authentication
    #   (Creates new user when logged in first time)
    def self.from_omniauth(auth_hash)
        user = find_or_create_by(uid: auth_hash['uid'], provider: auth_hash['provider'])
        user.name = auth_hash['info']['name']
        user.email = auth_hash['info']['email']
        user.location = auth_hash['info']['location']
        user.image_url = auth_hash['info']['image']
        user.url = auth_hash['info']['urls'][user.provider.capitalize]
        user.save!
        user
    end
end
