Rails.application.routes.draw do
  resources :books
  root to: 'books#index'
  
  get '/login', to: 'pages#index'
  get '/auth/:provider/callback', to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'
end
