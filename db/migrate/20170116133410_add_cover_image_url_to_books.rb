class AddCoverImageUrlToBooks < ActiveRecord::Migration[5.0]
  def change
    add_column :books, :cover_image_url, :string
  end
end
