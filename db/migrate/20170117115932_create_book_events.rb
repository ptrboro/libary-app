class CreateBookEvents < ActiveRecord::Migration[5.0]
  def change
    create_table :book_events do |t|
      t.references :book, foreign_key: true
      t.references :user, foreign_key: true
      t.date :borrow_date
      t.date :return_date

      t.timestamps
    end
  end
end
