desc "This task is called by the Heroku scheduler add-on"

task :send_book_reminders => :environment do
  Book.send_reminders
end