require "rails_helper"

RSpec.feature "Book Management" do
    
    before do
      Rails.application.env_config["omniauth.auth"] = OmniAuth.config.mock_auth[:google]
      visit "/auth/google"
    end
    
    scenario "User creates a new book successfully" do
        visit "/"
        click_link "Add Book"
        
        fill_in "Title", with: "Test Book Title"
        fill_in "Author", with: "Test Author"
        fill_in "Description", with: "Lorem ipsum"
        attach_file(:book_cover_image_url, "spec/files/book_cover.jpg")
        
        click_button 'Create Book'
        
        expect(page).to have_content("Test Book Title")
        expect(page).to have_content("Test Author")
        expect(page).to have_content("Lorem ipsum")
        expect(page).to have_xpath("//img[contains(@src,'book_cover.jpg')]")
        
        visit '/'
        expect(page).to have_xpath("//img[contains(@src,'book_cover.jpg')]")
    end
    
    scenario "User creates a new book not successfully" do
        visit "/"
        click_link "Add Book"
        
        fill_in "Title", with: ""
        fill_in "Author", with: ""
        fill_in "Description", with: ""
        
        click_button 'Create Book'
        
        expect(page).to have_content("The form contains 4 errors.")
        expect(page).to have_content("Title can't be blank")
        expect(page).to have_content("Author can't be blank")
        expect(page).to have_content("Description can't be blank")
        expect(page).to have_content("Cover image must be added")
    end
    
    scenario "User deletes book" do
        book = Book.create(title: "Test Book", author: "Test Author", description: "Lorem ipsum", added_by_id: 1, cover_image_url: File.open("spec/files/book_cover.jpg"))
        
        visit book_path(book)
        
        click_link "Delete"
        
        expect(page).not_to have_xpath("//img[contains(@src,'book_cover.jpg')]")
    end
    
    scenario "User edits book" do
        book = Book.create(title: "Test Book", author: "Test Author", description: "Lorem ipsum", added_by_id: 1, cover_image_url: File.open("spec/files/book_cover.jpg"))
        
        visit book_path(book)
        
        click_link "Edit"
        
        fill_in "Title", with: "New Book Title"
        fill_in "Author", with: "New Author"
        fill_in "Description", with: "New Lorem ipsum"
        
        click_button "Update Book"
        
        expect(page).to have_content("Book was successfully updated.")
        expect(page).to have_content("New Book Title")
        expect(page).to have_content("New Author")
        expect(page).to have_content("New Lorem ipsum")
        expect(page).to have_xpath("//img[contains(@src,'book_cover.jpg')]")
    end
    
    scenario "User try to edit not his book" do
       second_user = User.create(name: "Second User", provider: "test", uid: "test")
       book = second_user.added_books.build(title: "Test Book", author: "Test Author", description: "Lorem ipsum", cover_image_url: File.open("spec/files/book_cover.jpg"))
       book.save
       
       visit book_path(book)
           expect(page).not_to have_content("Edit")
           expect(page).not_to have_content("Delete")
       
       visit edit_book_path(book)
           expect(page).to have_content("All Books")
    end
end