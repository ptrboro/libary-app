require "rails_helper"

RSpec.feature "User google authentication" do
    before do
      Rails.application.env_config["omniauth.auth"] = OmniAuth.config.mock_auth[:google]
    end
    
    scenario "User log in successfully" do
        visit "/"
        within("header") do
            click_link "Login with Google"
        end
        expect(page).to have_content("Welcome, John Smith!")
        expect(page).not_to have_content("Welcome in LibaryApp!")
    end
    
    scenario "User logs out" do
       visit "/"
       within("header") do
            click_link "Log Out"
       end
       
       expect(page).not_to have_content("John Smith")
       expect(page).to have_content("Welcome in LibaryApp!")
       expect(page).to have_content("Login with Googlera")
    end
    
    scenario "Not logged in try to visit books index"
        visit "/books"
        
        expect(page).not_to have_content "No books in libary"
        expect(page).to have_content "Welcome in LibaryApp!"
        expect(page).to have_content "Login with Google"
    end
end