# Preview all emails at http://localhost:3000/rails/mailers/book_mailer
class BookMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/book_mailer/reminder
  def reminder
    book = Book.first
    BookMailer.reminder(book)
  end

end
